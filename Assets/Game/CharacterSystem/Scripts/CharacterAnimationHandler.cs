﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterAnimationHandler : MonoBehaviour
{
    [SerializeField] private float animationTime = 0.6f;
    [SerializeField] private float newZposDifferance = 183;

    public void FirstStageFinishAnimation(float maxBackwardPoint)
    {
        transform.DOMoveX(0f, animationTime).OnComplete(() =>
        {
            var newZPos = maxBackwardPoint + newZposDifferance;
            var newAnimTime = animationTime * 6f;
            GameManager.Instance.StartTranslateAnim(newZposDifferance, newAnimTime);
            transform.DOMoveZ(newZPos, newAnimTime).OnComplete(() =>
            {
                GameManager.Instance.StageBlockCount = LevelManager.Instance.CurrentLevel.Stage2Count;
                GameManager.Instance.TotalCollectedBlockCount = 0;
                GameManager.Instance.GameStarted();
            });
        });
    }
}
