﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDriver : MonoBehaviour
{
    [SerializeField] private float SensitivityX;
    [SerializeField] private float SensitivityY;
    private Vector2 movementAmount;
    public Vector2 MovementAmount => movementAmount;

    public void Drive()
    {
        var drag = InputManager.ScreenTouching ? InputManager.DragDeltaCm : Vector2.zero;
        drag.x *= SensitivityX;
        drag.y *= SensitivityY;
        movementAmount = drag;
    }
}
