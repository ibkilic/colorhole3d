﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CharacterUIController : MonoBehaviour
{
    [SerializeField] private List<SpriteRenderer> CharSpriteRenderers;
    [SerializeField] private float animationTime;

    private void OnEnable()
    {
        GameManager.OnGameStarted += SpriteAnim;
    }
    private void OnDisable()
    {
        GameManager.OnGameStarted -= SpriteAnim;

    }
    private void SpriteAnim()
    {
        foreach (var item in CharSpriteRenderers)
        {
            item.DOFade(0f, animationTime).OnComplete(() =>
            {
                item.gameObject.SetActive(false);
            });
        }
    }
}
