﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerHandle : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Collectible"))
        {
            if (GameManager.Instance.CurrentState == GameStates.Gameplay)
            {
                VibrationManager.CollectBlockVibration();
                GameManager.Instance.CollectNewBlock();

            }
            Destroy(other.gameObject);
        }
        if (GameManager.Instance.CurrentState == GameStates.Gameplay && other.CompareTag("Obstacle"))
        {
            GameManager.Instance.GameOver();
            Destroy(other.gameObject);

        }
    }
}

