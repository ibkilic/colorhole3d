﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticFieldController : MonoBehaviour
{

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Collectible") || other.CompareTag("Obstacle"))
        {
            var blockController = other.GetComponent<BlockController>();
            blockController.Activate();
            var forceDirection = transform.position - other.transform.position;
            forceDirection = new Vector3(forceDirection.x, 0f, forceDirection.z);
            blockController.GiveForce(forceDirection);
        }

    }
}
