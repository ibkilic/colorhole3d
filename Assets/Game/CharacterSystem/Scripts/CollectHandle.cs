﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectHandle : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Collectible"))
        {
            if (GameManager.Instance.CurrentState == GameStates.Gameplay)
            {
                other.GetComponent<BlockController>().GiveForce(Vector3.down, 20f);
                other.gameObject.layer = LayerMask.NameToLayer("Collectible");
            }
            else if (GameManager.Instance.CurrentState == GameStates.StageComplated)
            {
                VibrationManager.CollectBlockVibration();
                other.GetComponent<BlockController>().BridgeBlockAnimationPlay(transform);
            }
        }
        if (GameManager.Instance.CurrentState == GameStates.Gameplay)
        {
            if (other.CompareTag("Obstacle"))
            {
                other.GetComponent<BlockController>().GiveForce(Vector3.down, 20f);
                other.gameObject.layer = LayerMask.NameToLayer("Collectible");

            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Collectible") || other.CompareTag("Obstacle"))
        {
            other.gameObject.layer = LayerMask.NameToLayer("Default");
        }

    }
}
