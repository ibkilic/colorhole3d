﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHandler : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private CharacterDriver characterDriver;
    [SerializeField] private CharacterAnimationHandler characterAnimationHandler;
    [SerializeField] private MeshRenderer characterBorderMeshRenderer;

    [Header("Movement")]
    [Space]
    [SerializeField] private float maxLeftPoint;
    [SerializeField] private float maxRightPoint;
    [SerializeField] private float maxBackwardPoint;
    [SerializeField] private float maxForwardPoint;

    private void OnEnable()
    {
        GameManager.OnTranslateAnim += UpdateBoundary;
    }
    private void OnDisable()
    {
        GameManager.OnTranslateAnim -= UpdateBoundary;
    }

    private void Start()
    {
        InitialAssigment();
    }
    private void Update()
    {
        if (GameManager.Instance.CurrentState == GameStates.Gameplay)
        {
            characterDriver.Drive();
            Move(characterDriver.MovementAmount);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            GameManager.Instance.StageCompleted();
        }
    }

    private void InitialAssigment()
    {
        var startPos = new Vector3(0f, 0f, maxBackwardPoint);
        transform.position = startPos;
        characterBorderMeshRenderer.sharedMaterial = LevelManager.Instance.CurrentCharacterGroundMaterial;
    }
    private void Move(Vector2 amount)
    {
        var pos = transform.position;
        var posX = pos.x;
        var posZ = pos.z;
        posZ = Mathf.Clamp(posZ + amount.y, maxBackwardPoint, maxForwardPoint);
        posX = Mathf.Clamp(posX + amount.x, maxLeftPoint, maxRightPoint);
        pos.x = posX;
        pos.z = posZ;
        transform.position = pos;

    }
    private void UpdateBoundary(float newZPosDifferance, float arq0)
    {
        maxBackwardPoint += newZPosDifferance;
        maxForwardPoint += newZPosDifferance;
    }
    public void CharacterTranslateAnim()
    {
        characterAnimationHandler.FirstStageFinishAnimation(maxBackwardPoint);

    }
}
