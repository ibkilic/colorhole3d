﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WordManager : MonoSingleton<WordManager>
{
    [Header("Stage Variables")]
    [SerializeField] private Vector3 stage1Position;
    [SerializeField] private Vector3 stage2Position;
    [SerializeField] private Transform stageHolder;

    [Header("Environment")]
    [Space]
    [SerializeField] private GroundController groundController;
    [SerializeField] private DoorController doorController;


    [SerializeField] private ParticleSystem confettiParticle;

    public void CreateWorld()
    {
        CreateStages();
        SetLevelMaterials();
    }

    private void CreateStages()
    {
        var stage1 = Instantiate(LevelManager.Instance.CurrentStage1Template, Vector3.zero, Quaternion.identity);
        stage1.SetParent(stageHolder);
        stage1.name = "Stage 1";
        stage1.localPosition = stage1Position;

        var stage2 = Instantiate(LevelManager.Instance.CurrentStage2Template, Vector3.zero, Quaternion.identity);
        stage2.SetParent(stageHolder);
        stage2.name = "Stage 2";
        stage2.localPosition = stage2Position;
    }

    private void SetLevelMaterials()
    {
        groundController.SetGroundMaterials();
        doorController.SetDoorPartsMaterial();
    }
    public void PlayParticle()
    {
        confettiParticle.Play();
    }
}
