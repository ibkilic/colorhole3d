﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class DoorController : MonoBehaviour
{
    [SerializeField] private float animationTime = 0.6f;
    [SerializeField] private float lastYPos;
    [SerializeField] private Transform doorVisual;

    [SerializeField] private List<MeshRenderer> doorMeshRenderers;

    private void OnEnable()
    {
        GameManager.OnStageComplated += DoorOpenAnimation;
    }

    private void OnDisable()
    {
        GameManager.OnStageComplated -= DoorOpenAnimation;

    }
    private void DoorOpenAnimation()
    {
        doorVisual.DOMoveY(lastYPos, animationTime);
    }

    public void SetDoorPartsMaterial()
    {
        foreach (var item in doorMeshRenderers)
        {
            item.sharedMaterial = LevelManager.Instance.CurrentObstacleMaterial;
        }
    }
}

