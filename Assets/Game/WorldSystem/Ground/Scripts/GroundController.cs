﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    [SerializeField] private List<MeshRenderer> groundMeshRenderers;
    [SerializeField] private List<MeshRenderer> groundBorderMeshRenderers;

    [SerializeField] private Transform backgroundSprite;

    private void OnEnable()
    {
        GameManager.OnTranslateAnim += BackgroundSpriteAnim;
    }
    private void OnDisable()
    {
        GameManager.OnTranslateAnim -= BackgroundSpriteAnim;

    }

    private void BackgroundSpriteAnim(float zDistance, float animationTime)
    {
        var newZpos = backgroundSprite.position.z + zDistance;
        backgroundSprite.DOMoveZ(newZpos, animationTime);
    }

    public void SetGroundMaterials()
    {
        foreach (var item in groundMeshRenderers)
        {
            item.sharedMaterial = LevelManager.Instance.CurrentGroundMaterial;
        }
        foreach (var item in groundBorderMeshRenderers)
        {
            item.sharedMaterial = LevelManager.Instance.CurrentGroundBorderMaterial;
        }
    }
}
