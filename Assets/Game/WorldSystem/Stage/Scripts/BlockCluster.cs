﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCluster : MonoBehaviour
{
    [SerializeField] private List<GameObject> blockList;
    public int BlockCount => blockList.Count;
}
