﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public List<BlockCluster> BlockClusters;

    public int StageBlockCount
    {
        get
        {
            int totalBlockCount = 0;
            foreach (var blockCluster in BlockClusters)
            {
                totalBlockCount += blockCluster.BlockCount;
            }
            return totalBlockCount;
        }
    }
}
