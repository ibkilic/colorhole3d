﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlockController : MonoBehaviour
{
    [SerializeField] private Rigidbody blockRigidbody;

    [SerializeField] private float bridgeBlockAnimTime = 0.4f;

    public void Activate()
    {
        blockRigidbody.isKinematic = false;
    }
    public void GiveForce(Vector3 direction, float force = 30f)
    {
        blockRigidbody.AddForce(direction.normalized * force, ForceMode.Force);
    }

    public void BridgeBlockAnimationPlay(Transform character)
    {
        GetComponent<Collider>().isTrigger = true;
        var pos = character.position;
        pos.y = -3f;
        pos.x = transform.position.x;
        transform.DOMove(pos, bridgeBlockAnimTime);
    }
}
