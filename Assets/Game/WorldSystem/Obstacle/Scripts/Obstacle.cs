﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer.sharedMaterial = LevelManager.Instance.CurrentLevel.ObstacleMaterial;
    }
}
