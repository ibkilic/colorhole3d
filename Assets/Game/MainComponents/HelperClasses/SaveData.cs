﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class SaveData
{
    public static UnityAction OnCurrencyUpdate;

    private const string CurrentLevelKey = "CurrentLevel";
    public static int CurrentLevel
    {
        get => PlayerPrefs.GetInt(CurrentLevelKey, 0);
        set => PlayerPrefs.SetInt(CurrentLevelKey, value);
    }

    private const string CurrentGoldKey = "CurrentGold";
    public static int CurrentGold
    {
        get => PlayerPrefs.GetInt(CurrentGoldKey, 0);
        set => PlayerPrefs.SetInt(CurrentGoldKey, value);
    }
    private const string VibratinOnKey = "VibratinOn";
    public static bool VibrationOn
    {
        get => PlayerPrefsX.GetBool(VibratinOnKey, true);
        set => PlayerPrefsX.SetBool(VibratinOnKey, value);
    }

}
