﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoSingleton<InputManager>
{
    private Vector2 dragDeltaReading;
    private Vector2 previousMousePos;
    private Vector2 touchPosReading;
    private bool touchingReading;

    public static bool ScreenTouching { get { return Instance.touchingReading; } }

    public static Vector2 DragDeltaCm
    {
        get
        {
            var dragDelta = Instance.dragDeltaReading;
            var dragDeltaCm = new Vector2(dragDelta.x / Screen.dpi, dragDelta.y / Screen.dpi) * 2.54f;
            return dragDeltaCm;
        }
    }


    private void Start()
    {
        touchingReading = false;

    }
    private void Update()
    {
        HandleTouch();
    }


    public void HandleTouch()
    {
        touchingReading = false;
        dragDeltaReading = Vector2.zero;
        touchPosReading = Vector2.zero;


        if (Input.touchCount > 0)
        {
            touchingReading = true;
            var mainTouch = Input.touches[0];
            if (mainTouch.phase == TouchPhase.Moved || mainTouch.phase == TouchPhase.Ended || mainTouch.phase == TouchPhase.Canceled)
            {
                dragDeltaReading = mainTouch.deltaPosition;
            }
            touchPosReading = mainTouch.position;
        }


#if UNITY_EDITOR

        var mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        dragDeltaReading = mousePos - previousMousePos;
        touchPosReading = Input.mousePosition;

        if (Input.GetMouseButton(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                touchingReading = true;

            }
        }

        previousMousePos = Input.mousePosition;


#endif
    }

}
