﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VibrationManager : MonoSingleton<VibrationManager>
{
    [SerializeField] private Sprite vibrationOn, vibrationOff;

    public static void CollectBlockVibration()
    {
        Taptic.Medium();
    }
    public static void GameOverVibration()
    {
        Taptic.Failure();
    }
    public static void LevelCompleteVibration()
    {
        Taptic.Success();
    }

    public void SetCurrentSprite(Image vibrationImage)
    {
        if (SaveData.VibrationOn)
        {
            vibrationImage.sprite = vibrationOn;
        }
        else
        {
            vibrationImage.sprite = vibrationOff;
        }
    }

    public void ChangeVibrationStats(Image vibrationImage)
    {
        if (SaveData.VibrationOn)
        {
            vibrationImage.sprite = vibrationOff;
            SaveData.VibrationOn = false;

        }
        else
        {
            vibrationImage.sprite = vibrationOn;
            SaveData.VibrationOn = true;
        }
        Taptic.tapticOn = SaveData.VibrationOn;
    }

}
