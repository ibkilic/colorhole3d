﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public enum GameStates
{
    LoadingLevel,
    MainMenu,
    StageComplated,
    Gameplay,
    GameOver,
    LevelCompleted
}

public class GameManager : MonoSingleton<GameManager>
{
    public static UnityAction OnGameStarted, OnGameSceneLoad;



    public static UnityAction OnGameOver, OnLevelComplete, OnStageComplated;
    public static UnityAction<float, float> OnTranslateAnim;

    public static UnityAction<GameStates> OnGameStateChange;

    private GameStates currentState = GameStates.LoadingLevel;

    public GameStates CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            currentState = value;
            OnGameStateChange?.Invoke(value);
        }
    }


    public int TotalCollectedBlockCount;
    public int StageBlockCount;
    private bool isStage1Completed = false;

    [SerializeField] private CharacterHandler characterHandler;
    private new void Awake()
    {
        base.Awake();
        OnGameSceneLoad?.Invoke();
    }

    // Use this for initialization
    IEnumerator Start()
    {
        yield return null;
        CreateWorld();
        CurrentState = GameStates.MainMenu;
    }


    public void GameStarted()
    {
        OnGameStarted?.Invoke();
        CurrentState = GameStates.Gameplay;
    }

    public void GameOver()
    {
        OnGameOver?.Invoke();
        CurrentState = GameStates.GameOver;
        VibrationManager.GameOverVibration();
    }
    public void StartTranslateAnim(float ZposDifferance, float animationTime)
    {
        OnTranslateAnim?.Invoke(ZposDifferance, animationTime);

    }
    public void LevelCompleted()
    {

        SaveData.CurrentGold += 50;
        SaveData.CurrentLevel++;
        WordManager.Instance.PlayParticle();
        OnLevelComplete?.Invoke();
        CurrentState = GameStates.LevelCompleted;
        VibrationManager.LevelCompleteVibration();
    }

    public void StageCompleted()
    {
        isStage1Completed = true;
        characterHandler.CharacterTranslateAnim();
        OnStageComplated?.Invoke();
        CurrentState = GameStates.StageComplated;
    }

    private void CreateWorld()
    {
        WordManager.Instance.CreateWorld();
        StageBlockCount = LevelManager.Instance.CurrentLevel.Stage1Count;

    }
    public void CollectNewBlock()
    {
        TotalCollectedBlockCount++;
        UIManager.Instance.ProgressBarChange();
        if (TotalCollectedBlockCount >= StageBlockCount)
        {
            if (!isStage1Completed)
            {
                StageCompleted();
            }
            else
            {
                LevelCompleted();
            }
        }
    }

}
