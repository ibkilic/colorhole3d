﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraManager : MonoSingleton<CameraManager>
{
    [SerializeField] private float cameraShakeAnimationTime = 3f;
    private void OnEnable()
    {
        GameManager.OnTranslateAnim += StartTranslateAnim;
        GameManager.OnGameOver += CameraShake;

    }
    private void OnDisable()
    {
        GameManager.OnTranslateAnim -= StartTranslateAnim;
        GameManager.OnGameOver -= CameraShake;

    }


    private new void Awake()
    {
        base.Awake();
        //if dpi=0.66f ypos= 82;
        float cameraAspect = Camera.main.aspect;
        var heighRatio = 1920f / Screen.height;
        var widthRatio = 1280f / Screen.width;

        float ratio = widthRatio / heighRatio;
        float newY = (0.66f / cameraAspect) * 82f;


        var pos = transform.position;
        pos.y = newY;
        transform.position = pos;
    }
    private void StartTranslateAnim(float newZPosDifferance, float animationTime)
    {
        var newZpos = transform.position.z + newZPosDifferance;
        transform.DOMoveZ(newZpos, animationTime);
    }
    private void CameraShake()
    {
        StartCoroutine(ShakeRoutine());

    }
    private IEnumerator ShakeRoutine()
    {
        var pos = transform.position;
        pos.x = 0.5f;
        transform.position = pos;
        transform.DOMoveX(-0.5f, 0.05f).SetLoops(-1, LoopType.Yoyo).SetId("CameraShakeTween");
        yield return new WaitForSeconds(cameraShakeAnimationTime);
        DOTween.Kill("CameraShakeTween");
        transform.DOMoveX(0f, 0.05f);

    }
}
