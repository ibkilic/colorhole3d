﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private GameObject mainMenuPanel, gamePlayPanel, gameOverPanel, levelCompletePanel;
    [SerializeField] private GamePlayUIController gamePlayUIController;
    [SerializeField] private Image fadeImage;
    private GameObject currentPanel;
    private float fadeScreenAnimationTime = 0.5f;

    private void OnEnable()
    {
        fadeImage.gameObject.SetActive(true);
        gameOverPanel.GetComponent<CanvasGroup>().alpha = 0;
        levelCompletePanel.GetComponent<CanvasGroup>().alpha = 0;
        levelCompletePanel.SetActive(false);
        gameOverPanel.SetActive(false);

        GameManager.OnGameStateChange += ChangeUIPanels;
    }
    private void OnDisable()
    {
        GameManager.OnGameStateChange -= ChangeUIPanels;

    }

    private void ChangeUIPanels(GameStates gameState)
    {
        switch (gameState)
        {
            case GameStates.MainMenu:
                fadeImage.DOFade(0f, fadeScreenAnimationTime).SetEase(Ease.Linear).OnComplete(() =>
                {
                    fadeImage.gameObject.SetActive(false);
                });
                mainMenuPanel.SetActive(true);
                currentPanel = mainMenuPanel;
                break;
            case GameStates.Gameplay:
                if (currentPanel != gamePlayPanel)
                {
                    currentPanel.GetComponent<CanvasGroup>().DOFade(0f, fadeScreenAnimationTime).OnComplete(() =>
                     {
                         currentPanel.SetActive(false);
                         gamePlayPanel.SetActive(true);
                         currentPanel = gamePlayPanel;

                     });
                }
                break;
            case GameStates.GameOver:
                gameOverPanel.SetActive(true);
                gameOverPanel.GetComponent<CanvasGroup>().DOFade(1f, fadeScreenAnimationTime).OnComplete(() =>
                {
                    currentPanel = gameOverPanel;


                });
                break;
            case GameStates.LevelCompleted:
                levelCompletePanel.SetActive(true);
                levelCompletePanel.GetComponent<CanvasGroup>().DOFade(1f, fadeScreenAnimationTime).OnComplete(() =>
                {
                    currentPanel = levelCompletePanel;

                });
                break;
        }
    }

    public void ProgressBarChange()
    {
        gamePlayUIController.ProgressFilledImage();
    }
}
