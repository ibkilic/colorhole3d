﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUIController : MonoBehaviour
{
    [SerializeField] Image vibrationImage;
    private bool isSettingsPanelOpen = false;
    [SerializeField] private GameObject startButton;

    private void Start()
    {
        InitialAssigment();
    }

    private void InitialAssigment()
    {
        isSettingsPanelOpen = false;
        vibrationImage.gameObject.SetActive(false);
        VibrationManager.Instance.SetCurrentSprite(vibrationImage);
    }
    public void ClickSettingsButton()
    {
        isSettingsPanelOpen = !isSettingsPanelOpen;
        vibrationImage.gameObject.SetActive(isSettingsPanelOpen);

    }

    public void ChangeVibrationStats()
    {
        VibrationManager.Instance.ChangeVibrationStats(vibrationImage);
    }


    public void StartGame()
    {
        startButton.SetActive(false);
        GameManager.Instance.GameStarted();
    }
}
