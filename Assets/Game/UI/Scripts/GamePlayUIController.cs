﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GamePlayUIController : MonoBehaviour
{
    [SerializeField] private Image stage1FilledImage, stage2FilledImage;
    [SerializeField] private TextMeshProUGUI currentLevelText, nextLevelText, goldText;

    private Image currentFilledImage;

    private void OnEnable()
    {
        GameManager.OnStageComplated += SwitchFillImage;
    }
    private void OnDisable()
    {
        GameManager.OnStageComplated -= SwitchFillImage;

    }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        int level = SaveData.CurrentLevel;
        currentLevelText.text = $"{level + 1}";
        nextLevelText.text = $"{level + 2}";
        goldText.text = $"{SaveData.CurrentGold}";
        currentFilledImage = stage1FilledImage;

    }

    public void ProgressFilledImage()
    {
        var amount = Mathf.InverseLerp(0, GameManager.Instance.StageBlockCount, GameManager.Instance.TotalCollectedBlockCount);
        var value = Mathf.Clamp(amount, 0, 1);
        currentFilledImage.fillAmount = value;
    }

    private void SwitchFillImage()
    {
        currentFilledImage.fillAmount = 1;
        currentFilledImage = stage2FilledImage;
    }


}
