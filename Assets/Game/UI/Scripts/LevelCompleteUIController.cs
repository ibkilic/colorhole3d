﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCompleteUIController : MonoBehaviour
{
    public void NextLevel()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }
}
