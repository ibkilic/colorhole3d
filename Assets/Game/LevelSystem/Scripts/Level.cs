﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Level")]
public class Level : ScriptableObject
{
    [SerializeField] private Stage stage1;
    [SerializeField] private Stage stage2;

    public int Stage1Count => stage1.StageBlockCount;
    public int Stage2Count => stage2.StageBlockCount;

    public Transform Stage1Template => stage1.transform;
    public Transform Stage2Template => stage2.transform;

    public Material ObstacleMaterial;
    public Material GroundMaterial;
    public Material GroundBorderMaterial;
    public Material CharacterBorderMaterial;
}
