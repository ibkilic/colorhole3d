﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoSingleton<LevelManager>
{
    [SerializeField] private List<Level> levels;

    private int level => SaveData.CurrentLevel % levels.Count;

    public Level CurrentLevel => levels[level];

    public Transform CurrentStage1Template => CurrentLevel.Stage1Template;
    public Transform CurrentStage2Template => CurrentLevel.Stage2Template;

    public int CurrentStage1BlockCount => CurrentLevel.Stage1Count;
    public int CurrentStage2BlockCount => CurrentLevel.Stage2Count;

    public Material CurrentObstacleMaterial => CurrentLevel.ObstacleMaterial;
    public Material CurrentGroundMaterial => CurrentLevel.GroundMaterial;
    public Material CurrentGroundBorderMaterial => CurrentLevel.GroundBorderMaterial;
    public Material CurrentCharacterGroundMaterial => CurrentLevel.CharacterBorderMaterial;
}
